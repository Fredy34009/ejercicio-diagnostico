/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.Factura;
import com.diagnostico.modelo.ModoPago;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author FREDY
 */
public class ModoPagoFacade extends AbstractFacade<ModoPago> implements Dao<ModoPago> {

    private EntityManager em;

    public ModoPagoFacade() {
        super(ModoPago.class);
        em = Persistence.createEntityManagerFactory("conn").createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
