/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.Cliente;
import com.diagnostico.modelo.Factura;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author FREDY
 */
public class FacturaFacade extends AbstractFacade<Factura> implements Dao<Factura> {

    private EntityManager em;

    public FacturaFacade() {
        super(Factura.class);
        em = Persistence.createEntityManagerFactory("conn").createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void procedure(Factura fac) {
        try {
            em.getTransaction().begin();
            Query q = em.createNativeQuery("CALL sp_insercion_factura(?,?,?)");
            System.out.println(""+q.toString());
            q.setParameter(1, fac.getIdCliente().getIdCliente());
            q.setParameter(2, fac.getFecha());
            q.setParameter(3, fac.getNumPago().getNumPago());
            q.executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }
}
