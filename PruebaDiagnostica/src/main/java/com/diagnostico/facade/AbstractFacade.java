/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author FREDY
 */
public abstract class AbstractFacade<T> {

    private Class<T> entity;

    public AbstractFacade(Class<T> entity) {
        this.entity = entity;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        try {
            getEntityManager().getTransaction().begin();
            getEntityManager().persist(entity);
            getEntityManager().getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error guardando"+e.getMessage());
        }
    }

    public void edit(T entity) {
        getEntityManager().getTransaction().begin();
        getEntityManager().merge(entity);
        getEntityManager().getTransaction().commit();
    }

    public void remove(T entity) {
        getEntityManager().getTransaction().begin();
        getEntityManager().remove(getEntityManager().merge(entity));
        getEntityManager().getTransaction().commit();
    }

    public List<T> findAll() {
        CriteriaBuilder cb=getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq=cb.createQuery(entity);
        cq.select(cq.from(entity));
        return  getEntityManager().createQuery(cq).getResultList();
    }
}
