/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.Cliente;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author FREDY
 */
public class ClienteFacade extends AbstractFacade<Cliente> implements Dao<Cliente>{

    private EntityManager em;
    public ClienteFacade() {
        super(Cliente.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
