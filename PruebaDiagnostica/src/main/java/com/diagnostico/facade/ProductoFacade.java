/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.ModoPago;
import com.diagnostico.modelo.Producto;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author FREDY
 */
public class ProductoFacade extends AbstractFacade<Producto> implements Dao<Producto> {

    private EntityManager em;

    public ProductoFacade() {
        super(Producto.class);
        em = Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
