/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.Categoria;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author FREDY
 */
public class CategoriaFacade extends AbstractFacade<Categoria> implements Dao<Categoria>{

    private final EntityManager em;

    public CategoriaFacade() {
        super(Categoria.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    @Override
    protected EntityManager getEntityManager() {
        return  em;
    }
    
}
