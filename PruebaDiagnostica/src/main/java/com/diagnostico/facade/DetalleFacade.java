/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diagnostico.facade;

import com.diagnostico.modelo.Cliente;
import com.diagnostico.modelo.Detalle;
import com.diagnostico.modelo.Detalle_;
import com.diagnostico.modelo.Factura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

/**
 *
 * @author FREDY
 */
public class DetalleFacade extends AbstractFacade<Detalle> implements Dao<Detalle> {

    private EntityManager em;

    public DetalleFacade() {
        super(Detalle.class);
        em = Persistence.createEntityManagerFactory("conn").createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<Detalle> facturas() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Detalle> cq = cb.createQuery(Detalle.class);

        List<Detalle> list = em.createNativeQuery("select * from detalle "
                + " inner join factura on factura.num_factura=detalle.id_factura"
                + " group by id_factura order by id_cliente", Detalle.class).getResultList();

        return list;
    }

    public List<Detalle> productos(Detalle d) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Detalle> cq = cb.createQuery(Detalle.class);
        Root<Detalle> root = cq.from(Detalle.class);

        System.out.println("idFactura " + d.getFactura().getNumFactura());
        cq.where(cb.equal(root.get("factura"), d.getFactura()));

        return em.createQuery(cq).getResultList();
    }
}
