create database participante26;
use participante26;

create table modo_pago
( num_pago int primary key auto_increment not null,
nombre varchar(50) not null,
otros_detalles varchar(60) 
);
create table categoria
(id_categoria int primary key auto_increment not null,
nombre varchar(50) not null,
descripcion varchar(50)
);
create table producto(
id_producto int auto_increment not null,
nombre varchar(50) not null,
precio double(7,2) not null,
stock int not null,
primary key (id_producto),
id_categoria int not null,
foreign key(id_categoria) references categoria(id_categoria)
);
create table cliente(
id_cliente int primary key auto_increment not null,
nombre varchar(50) not null,
apellido varchar(50) not null,
direccion varchar(100) not null,
fecha_nacimiento date not null,
telefono varchar(12) ,
email varchar(250)
);
create table factura(
num_factura int auto_increment not null,
id_cliente int not null,
fecha date not null,
num_pago int not null,
primary key(num_factura),
foreign key(id_cliente) references cliente(id_cliente),
foreign key(num_pago) references modo_pago(num_pago)
);
create table detalle(
num_detalle int  not null auto_increment ,
id_factura int  not null,
id_producto int not null,
cantidad int not null,
precio double(7,2),
primary key (num_detalle,id_factura)
);
alter table detalle add constraint dwefefe foreign key(id_producto) references producto(id_producto);
alter table detalle add constraint forevcv foreign key(id_factura) references factura(num_factura);

/*Procedimiento almacenado */
DELIMITER //
create procedure sp_insercion_factura ( in id_cliente int, in fecha date,in num_pago int)
BEGIN
	insert into factura(id_cliente,fecha,num_pago) values (id_cliente,fecha,num_pago);
END //
DELIMITER ;

/* Trigger  */
DELIMITER //
create trigger tr_dismimuye_stock
after insert on detalle for each row
begin 
	update producto set stock=stock-new.cantidad where id_producto=new.id_producto;
end//